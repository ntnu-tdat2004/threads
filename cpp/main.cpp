#include <iostream>
#include <thread>
#include <vector>

using namespace std;

int main() {
  vector<thread> threads;

  for (int i = 0; i < 10; i++) {
    threads.emplace_back([i] {                    // i is copied to the thread, do not capture i as reference (&i) as it might be freed before all the threads finishes.
      cout << "Output from thread " << i << endl; // The output might be interleaved since the string, integer and newline are output in succession.
    });
  }

  for (auto &thread : threads)
    thread.join();
}
